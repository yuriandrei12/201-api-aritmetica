package com.br.itau.services;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class MatematicaService {

    public int soma(List<Integer> numeros){
        int resultado = 0;
        for(int numero : numeros){
            resultado += numero;
        }
        return resultado;
    }

    public int subtracao(List<Integer> numeros){
        int resultado = 0;
        for (int numero : numeros){
            if(resultado == 0){
                resultado = numero;
            } else{
                resultado -= numero;
            }
        }
        return resultado;
    }

    public int multiplicacao(List<Integer> numeros){
        int resultado = 0;
        for (int numero : numeros){
            if (resultado == 0){
                resultado = numero;
            } else {
                resultado *= numero;
            }
        }
        return resultado;
    }

    public int divisao(List<Integer> numeros){
        int resultado = 0;
        for (int numero : numeros){
            if (numero == 0){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Não existe divisão de zeros, seu burro");
            } else if (resultado == 0){
                resultado = numero;
            } else if (resultado < numero) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "O numero a ser dividido não pode ser menor que o seu divisor, não é possível dividir " + resultado + " por " + numero);
            } else {
                resultado /= numero;
            }
        }
        return resultado;
    }
}